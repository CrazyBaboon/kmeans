# KMeans

Python utility that performs k-means clustering on a given PNG image.

Input:

![alt text](img/test.png)

Output (5 clusters):

![alt text](img/output_kmeans.png)