#(C) 2018 Nuno Ferreira - MIT License

from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from PIL import Image
import pandas as pd
import numpy as np


img = Image.open('test.png')
img = img.convert('RGB')
img = img.rotate(180) # the .png is originaly represented upside down

size_x = img.size[0] 
size_y = img.size[1] 

#Count non-zero pixels:

sample_size = 0 
 
for n in range(0, size_x):
    for l in range(0, size_y):
        if (img.getpixel((n,l))[1] != 0):
            sample_size = sample_size + 1

#Get the coordinates of the meaningful pixels:

data_x = np.zeros(sample_size)
data_y = np.zeros(sample_size)
    
dummy = 0
for n in range(0, size_x):
    for l in range(0, size_y):
        if (img.getpixel((n,l))[1] != 0):
            data_x[dummy] = n
            data_y[dummy] = l
            dummy += 1

plt.scatter(data_x,data_y)
plt.show()

#Now predict the clusters using KMeans:

df = pd.DataFrame( {'x_pos': data_x[:], 'y_pos': data_y[:]} )
kmeans = KMeans(n_clusters = 5)
kmeans.fit(df)
labels = kmeans.predict(df)
plt.scatter(data_x,data_y,c = labels)
